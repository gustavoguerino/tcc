\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{13}
\contentsline {section}{\numberline {1.1}Objetivos}{14}
\contentsline {subsection}{\numberline {1.1.1}Objetivos Gerais}{14}
\contentsline {subsection}{\numberline {1.1.2}Objetivos Espec\IeC {\'\i }ficos}{14}
\contentsline {chapter}{\numberline {2}Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}{15}
\contentsline {section}{\numberline {2.1}Internet das coisas}{15}
\contentsline {section}{\numberline {2.2}Raspberry Pi}{15}
\contentsline {subsection}{\numberline {2.2.1}GPIO}{16}
\contentsline {section}{\numberline {2.3}Message Queuing Telemetry Transport (MQTT)}{16}
\contentsline {subsection}{\numberline {2.3.1}Modelo publica\IeC {\c c}\IeC {\~a}o e assinatura}{17}
\contentsline {subsection}{\numberline {2.3.2}Mosquitto (Broker)}{18}
\contentsline {section}{\numberline {2.4}Hardware Utilizado}{18}
\contentsline {subsection}{\numberline {2.4.1}Sensor de temperatura e humidade DHT22}{18}
\contentsline {subsection}{\numberline {2.4.2}Protoboard}{18}
\contentsline {subsection}{\numberline {2.4.3}Buzzer}{19}
\contentsline {subsection}{\numberline {2.4.4}Modulo sensor de som KY-037}{19}
\contentsline {section}{\numberline {2.5}Tecnologias Adotadas}{20}
\contentsline {subsection}{\numberline {2.5.1}Python}{20}
\contentsline {chapter}{\numberline {3}Cap\IeC {\'\i }tulo 3}{21}
\contentsline {section}{\numberline {3.1}Se\IeC {\c c}\IeC {\~a}o 1}{22}
\contentsline {section}{\numberline {3.2}Se\IeC {\c c}\IeC {\~a}o 2}{22}
\contentsline {subsection}{\numberline {3.2.1}Subse\IeC {\c c}\IeC {\~a}o 2.1}{23}
\contentsline {subsection}{\numberline {3.2.2}Subse\IeC {\c c}\IeC {\~a}o 2.2}{23}
\contentsline {section}{\numberline {3.3}Se\IeC {\c c}\IeC {\~a}o 3}{23}
\contentsline {section}{\numberline {3.4}Se\IeC {\c c}\IeC {\~a}o 4}{23}
\contentsline {chapter}{\numberline {4}Cap\IeC {\'\i }tulo 4}{24}
\contentsline {section}{\numberline {4.1}Se\IeC {\c c}\IeC {\~a}o 1}{24}
\contentsline {section}{\numberline {4.2}Se\IeC {\c c}\IeC {\~a}o 2}{24}
\contentsline {chapter}{\numberline {5}Cap\IeC {\'\i }tulo 5}{25}
\contentsline {section}{\numberline {5.1}Se\IeC {\c c}\IeC {\~a}o 1}{25}
\contentsline {section}{\numberline {5.2}Se\IeC {\c c}\IeC {\~a}o 2}{25}
\contentsline {subsection}{\numberline {5.2.1}Subse\IeC {\c c}\IeC {\~a}o 5.1}{25}
\contentsline {subsection}{\numberline {5.2.2}Subse\IeC {\c c}\IeC {\~a}o 5.2}{26}
\contentsline {section}{\numberline {5.3}Se\IeC {\c c}\IeC {\~a}o 3}{26}
\contentsline {chapter}{\numberline {6}Considera\IeC {\c c}\IeC {\~o}es finais}{27}
\contentsline {section}{\numberline {6.1}Principais contribui\IeC {\c c}\IeC {\~o}es}{27}
\contentsline {section}{\numberline {6.2}Limita\IeC {\c c}\IeC {\~o}es}{27}
\contentsline {section}{\numberline {6.3}Trabalhos futuros}{27}
\contentsline {chapter}{Refer\^encias}{28}
\contentsline {chapter}{Ap\^endice{} A{} -$\tmspace -\thinmuskip {.1667em}$-{} Primeiro ap\IeC {\^e}ndice}{30}
\contentsline {chapter}{Anexo{} A{} -$\tmspace -\thinmuskip {.1667em}$-{} Primeiro anexo}{31}
